#!/usr/bin/env python

from __future__ import print_function

from distutils.core import setup

from upenn_isc_aws import __version__

import sys

if sys.version_info <= (2, 5):
    print("ERROR: This code requires at least version 2.6 of python",
          file=sys.stderr)
    sys.exit(1)
                
def long_description():
    with open("README.md") as f:
        return f.read()

setup(name         = "upenn_isc_aws",
      version      = __version__,
      description  = "Example python scripts automating AWS infrastructure",
      long_description = long_description(),
      author       = "Michel van der List",
      author_email = "Michel@hashedbytes.com",
      scripts      = ["bin/aws_clone_ec2_instance", "bin/aws_create_vpc",
                      "bin/aws_download_bucket_to_dir",
                      "bin/aws_run_instances", "bin/aws_send_sns_message",
                      "bin/aws_show_availability_zones",
                      "bin/aws_show_buckets", "bin/aws_show_images",
                      "bin/aws_show_instances",
                      "bin/aws_show_instances_pprint", "bin/aws_show_security", "bin/aws_start_stop_instance",
                      "bin/aws_tag_resource", "bin/aws_upload_dir_as_bucket"],
      url          = "https://bitbucket.org/mjv39474/aws-scripting-examples",
      packages     = ["upenn_isc_aws"],
      classifiers  = [ "Intended Audience :: Developers",
                       "License :: OSI Approved :: MIT License",
                       "Operating System :: OS Independent",
                       "Programming Language :: Python",
                       "Programming Language :: Python :: 2.6",
                       "Programming Language :: Python :: 2.7",
                       "Topic :: Software Development :: Libraries :: Python Modules",
                       "Development Status :: 4 - Beta",
                       "Intended Audience :: Developers",
                       "Intended Audience :: Education" ],
      )
