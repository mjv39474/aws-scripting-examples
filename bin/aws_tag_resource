#!/usr/bin/env python
"""
Example 6:

Many resources in AWS can be tagged. At times the tag has a
special meaning, such as the tag called 'Name'.
"""
from __future__ import print_function

import argparse
import boto.ec2

options = None

def main():
    #pylint: disable-msg=C0111
    global options
    parser = argparse.ArgumentParser(
        description="Tag an AWS resoruce.",
        usage=("%(prog)s [optional arguments] <resource-id> <key> <value>\n" +
               "       e.g. use the key 'Name' to name a resource"))
    parser.add_argument("-r", "--regionname", default='us-east-1',
        help="The AWS region to use (e.g. us-east-1).")
    parser.add_argument("resourceid",
        help="The id of the resource to tag.")
    parser.add_argument("key",
        help="The key name of the tag.")
    parser.add_argument("value",
        help="The value associated with the key name.")
    options = parser.parse_args()

    regions = [r.name for r in boto.ec2.regions()]
    if not options.regionname in regions:
        raise RuntimeError(options.regionname +
            " not found, valid regions are: " + ", ".join(regions))
    conn = boto.ec2.connect_to_region(options.regionname)

    # It's just a single call...
    conn.create_tags(options.resourceid, {options.key: options.value})

if __name__ == '__main__':
    main()
