#!/bin/bash

function _awsrc_completion_()
{
    local word=${COMP_WORDS[COMP_CWORD]}
    local aws_loc="$HOME/.aws"
    local l
    
    l=`/bin/ls -1 $aws_loc/awsrc-${word}*.cfg 2>/dev/null`
    for f in $l; do
	f=${f##*/awsrc-}
        f=${f%.cfg}
        COMPREPLY+=($f)
    done
}

complete -F _awsrc_completion_ switch-aws
