Some example AWS scripting code for a presentation.

Requires boto: https://github.com/boto/boto

You will need to set up AWS API access for this to               
function. Getting started with boto explains this:               
http://docs.pythonboto.org/en/latest/getting_started.html        

