#!/usr/bin/python
from __future__ import print_function

import logging
import sys
import time

def spinner():
    def spinit():
        sys.stdout.write(spinit.phases[spinit.count] + "\r")
        spinit.count = (spinit.count + 1) % len(spinit.phases)
        sys.stdout.flush()
    spinit.count = 0
    spinit.phases = ('-', '\\', '|', '/')
    return spinit

def wait_for_object(obj, attr='status', message='completed', tries=10,
                    delay=1, progress=False):
    count = 0
    if progress:
        spin = spinner()
    mess = getattr(obj, attr)
    while mess != message:
        obj.update()
        mess = getattr(obj, attr)
        if mess != message and count < tries:
            count += 1
            if progress:
                spin()
            logging.info("Waiting for completion of " + obj.id + " (" +
                message + " currently " + mess + ") - " + str(count) +
                " of " + str(tries) + " tries...")
            time.sleep(delay)
    if mess != message:
        m = obj.id + " failed to reach '" + message + "'"
        logging.critical(m)
        raise RuntimeError(m)

def xstr(s):
    return '' if s is None else str(s)
